**Setup**

**Precondition:** Should have the following in the PC: Selenium IDE with TestNG and chromedriver. Make sure the chromedriver version is same with your current browser in your PC, otherwise, it might affect the chromedriver and will cause an error once it is not same with the current browser that you were using.

1. Access Gitlab URL https://gitlab.com/nikkibealim/for-exam
2. Once you're in the For exam Repo, you will see Automation Exam.zip and README.md files
3. Read the README.md for the instruction on how to replicate the setup
4. In Automation Exam.zip, this contains the scenarios that I've converted in Selenium scripts, click this and click download
5. Extract the zip file in your PC
6. Once the zip is downloaded, import it in the Selenium by clicking File > Import > General > File System > Browse the file where you extracted the file and select it > Select the file from the File system > Click Finish
7. Once you have successfully imported it in Selenium, you will see the Project name Test and the package name is selenium. Under selenium package it has 2 java classess, Home_Properties.java and Dropdown.java. Home_Properties.java is the main class while the Dropdown.java is where all the scenarios are placed
8. When running this test, right click the selenium package and select Run as 2 TestNG Test
9. Google chrome should automatically open and you will see the https://qaexam.forge99.com/ website

Reference for the Software needed to run the script:

- **Eclipse** (Oxygen version): https://www.eclipse.org/downloads/packages/release/oxygen **Note:** Select 3A Packages.
- **Selenium WebDriver:** https://www.selenium.dev/downloads/ **Note:** Select Java under Selenium Clients and WebDriver Language Bindings.
- **ChromeDriver:** https://chromedriver.chromium.org/
- **TestNG**: This can be downloaded inside Eclipse. Just click Help > Eclipse Market > Search for TestNG > Click Install > Click Confirm > Click Accept Terms & Conditions > Click Finish

**Note: The script will run in sequential based on the scenarios provided on the exam since I have annotations in scripts which is @Test.**
